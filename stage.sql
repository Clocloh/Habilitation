-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 13 Avril 2018 à 09:27
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stage`
--

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_bdd`
--

CREATE TABLE `entreprise_bdd` (
  `id` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `nom_bdd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise_bdd`
--

INSERT INTO `entreprise_bdd` (`id`, `id_entreprise`, `nom_bdd`) VALUES
(1, 38, 'Array'),
(2, 38, 'Array'),
(3, 38, 'Array'),
(4, 39, 'OVH'),
(5, 39, 'SQLServer'),
(6, 39, 'ORACLE'),
(7, 41, 'OVH'),
(8, 41, 'SQLServer'),
(9, 42, 'SQLServer');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_langage`
--

CREATE TABLE `entreprise_langage` (
  `id` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `langage` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise_langage`
--

INSERT INTO `entreprise_langage` (`id`, `id_entreprise`, `langage`) VALUES
(1, 37, 'CSS'),
(2, 37, 'C#'),
(3, 37, 'JAVA'),
(4, 38, 'C#'),
(5, 38, ' C++'),
(6, 38, 'JAVA'),
(7, 41, 'PHP'),
(8, 41, 'C#'),
(9, 41, ' C++'),
(10, 42, ' C++');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_methode`
--

CREATE TABLE `entreprise_methode` (
  `id` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `methode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise_methode`
--

INSERT INTO `entreprise_methode` (`id`, `id_entreprise`, `methode`) VALUES
(1, 40, 'AGILE'),
(2, 40, 'En V'),
(3, 41, 'AGILE'),
(4, 41, 'En V'),
(5, 42, 'En V');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_modele`
--

CREATE TABLE `entreprise_modele` (
  `id` int(255) NOT NULL,
  `nom_ent` varchar(50) NOT NULL,
  `raison_sociale` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `cp` int(100) NOT NULL,
  `email_ent` varchar(100) NOT NULL,
  `num_tel` int(10) NOT NULL,
  `site` varchar(100) NOT NULL,
  `intitule` varchar(100) NOT NULL,
  `niveau` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise_modele`
--

INSERT INTO `entreprise_modele` (`id`, `nom_ent`, `raison_sociale`, `adresse`, `ville`, `cp`, `email_ent`, `num_tel`, `site`, `intitule`, `niveau`) VALUES
(26, 'gt', 'dd', 'rr', 'yy', 45, 'dfgdddg@mgmg.glgl', 54454, 'dfsdfsd', 'fsfsd', ''),
(27, '', '', '', '', 0, '', 0, '', '', ''),
(28, 'bf', '424', 'fds', 'fds', 75018, 'naem@gkfg.fr', 5555, 'fdsfd.cc', 'fdfd', 'bac1'),
(29, '', '5287', '', '', 0, 'naem@gkfg.fr', 5555, '', 'fdfd', 'bac1'),
(30, 'ghgdf', '424', 'fds', '', 0, 'naem@gkfg.fr', 5555, 'fdsfd.cc', 'fdfd', 'bac1'),
(31, 'ghgdf', 'fvdzq', 'fds', 'fds', 75018, 'naem@gkfg.fr', 5555, 'fdsfd.cc', 'gres', 'bac2'),
(32, 'ghgdf', 'fvdzq', '', '', 0, 'naem@gkfg.fr', 5555, 'fdsfd.cc', 'fdfd', 'bac1'),
(33, 'bf', '424', 'fds', 'fds', 75018, 'naem@gkfg.fr', 5555, 'fdsfd.cc', 'fdfd', 'bac1'),
(34, '', 'fvdzq', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'gres', 'bac1'),
(35, '', 'fvdzq', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'fdfd', 'bac1'),
(36, '', 'Munivie', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'Stage PHP POO', 'bac1'),
(37, '', 'Munivie', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'Stage PHP POO', 'bac1'),
(38, '', 'Munivie', '', '', 0, 'naem@gkfg.fr', 5555, 'www.suadeo.fr', 'Stage PHP POO', 'bac1'),
(39, '', 'Munivie', 'fds', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'gres', 'bac1'),
(40, '', 'Munivie', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'Stage PHP POO', 'bac1'),
(41, '', 'Munivie', '', '', 0, 'naem@gkfg.fr', 0, 'www.suadeo.fr', 'Stage PHP POO', 'bac1'),
(42, '', 'Munivie', 'fds', '', 0, 'naem@gkfg.fr', 5555, 'www.suadeo.fr', 'Stage PHP POO', 'bac1');

-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

CREATE TABLE `offre` (
  `id` int(11) NOT NULL,
  `Nom_ent` varchar(255) NOT NULL,
  `Telephone` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `Code_Postal` int(5) NOT NULL,
  `Details` varchar(255) NOT NULL,
  `Niveau` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `entreprise_bdd`
--
ALTER TABLE `entreprise_bdd`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `entreprise_langage`
--
ALTER TABLE `entreprise_langage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `entreprise_methode`
--
ALTER TABLE `entreprise_methode`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `entreprise_modele`
--
ALTER TABLE `entreprise_modele`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `offre`
--
ALTER TABLE `offre`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `entreprise_bdd`
--
ALTER TABLE `entreprise_bdd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `entreprise_langage`
--
ALTER TABLE `entreprise_langage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `entreprise_methode`
--
ALTER TABLE `entreprise_methode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `entreprise_modele`
--
ALTER TABLE `entreprise_modele`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT pour la table `offre`
--
ALTER TABLE `offre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
