-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 13 avr. 2018 à 11:47
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppe`
--

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `ID` int(11) NOT NULL,
  `nom` varchar(60) COLLATE latin1_general_cs NOT NULL,
  `prenom` varchar(60) COLLATE latin1_general_cs NOT NULL,
  `username` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Entreprise` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `telephone` int(255) NOT NULL,
  `adresse` int(11) NOT NULL,
  `Siren` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`ID`, `nom`, `prenom`, `username`, `password`, `Entreprise`, `email`, `telephone`, `adresse`, `Siren`) VALUES
(0, '', '', 'greg', '79e2475f81a6317276bf7cbb3958b20d289b78df', '', 'greg@gmail.com', 0, 0, 779727),
(0, '', '', 'bqty', 'a4bcd0fffc40b7ff4a9a288bdd53a798abfc043d', '', 'greg@gmail.com', 0, 0, 8562),
(0, 'alain', 'bqty', 'layn', '5fcd2d7942c5480f3580925696e57da42e415eff', '', 'alafootom@msn.com', 0, 0, 5489),
(0, 'alain', 'bqty', 'layn', '5fcd2d7942c5480f3580925696e57da42e415eff', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 5489),
(0, 'alain', 'bqty', 'layn', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 5489),
(0, 'alain', 'bqty', 'layn', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', 'alafootom@msn.com', 0, 0, 5489),
(0, 'alain', 'bqty', 'layn', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 54894);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
