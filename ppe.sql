-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 22 mai 2018 à 10:33
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppe`
--

-- --------------------------------------------------------

--
-- Structure de la table `eleves`
--

CREATE TABLE `eleves` (
  `ID` int(11) NOT NULL,
  `username` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Prenom` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Nom` varchar(11) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(99) COLLATE latin1_general_cs NOT NULL,
  `adresse` varchar(99) COLLATE latin1_general_cs NOT NULL,
  `telephone` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Déchargement des données de la table `eleves`
--

INSERT INTO `eleves` (`ID`, `username`, `password`, `Prenom`, `Nom`, `email`, `adresse`, `telephone`) VALUES
(1, '3', '4', '2', '1', '5', '7', 6),
(2, '0', '0', '0', '0', 'greg@gmail.com', '89 av noruto', 665265987),
(3, '0', '0', '0', '0', 'greg@gmail.com', '89 avenue de naruto', 665265987),
(4, 'pp', 'pp', 'pp', 'pp', 'alafootom@msn.com', '89 avenue de naruto', 665265987),
(5, 'layn', 'layn', 'Alain', 'Bouquety', 'abouquety1@gmail.com', '89 avenue de naruto', 665265987),
(6, 'sdffsd', 'sdfsdfsdf', 'sdfsdf', 'fdsf', 'greg@gmail.com', '89 avenue de naruto', 154789425),
(7, 'layn', '8450103c06dbd58add9d047d761684096ac560ca', 'greg', 'greg', 'greg@gmail.com', '52 allée des rossilons', 665265987),
(8, 'marjory', 'eda2a3ba365d48999328ac4148dc86ee0a393934', 'morgane', 'cassory', 'alafootom@msn.com', '52 allée des rossilons', 665265987),
(9, 'tim', '1bfbdf35b1359fc6b6f93893874cf23a50293de5', 'timothe', 'robert', 'greg@gmail.com', '52 allée des rossilons', 154789425),
(10, 'jj', '7323a5431d1c31072983a6a5bf23745b655ddf59', 'jj', 'jj', 'jj@jj.com', '', 0),
(11, 'jj', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'jj', 'jj', 'jj@jj.com', '', 0),
(12, 'jj', '7323a5431d1c31072983a6a5bf23745b655ddf59', 'jj', 'jj', 'jj@jj.com', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `ID` int(11) NOT NULL,
  `nom` varchar(60) COLLATE latin1_general_cs NOT NULL,
  `prenom` varchar(60) COLLATE latin1_general_cs NOT NULL,
  `username` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Entreprise` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `telephone` int(255) NOT NULL,
  `adresse` int(11) NOT NULL,
  `Siren` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`ID`, `nom`, `prenom`, `username`, `password`, `Entreprise`, `email`, `telephone`, `adresse`, `Siren`) VALUES
(0, '', '', 'greg', '79e2475f81a6317276bf7cbb3958b20d289b78df', '', 'greg@gmail.com', 0, 0, 779727),
(0, '', '', 'bqty', 'a4bcd0fffc40b7ff4a9a288bdd53a798abfc043d', '', 'greg@gmail.com', 0, 0, 8562),
(0, 'alain', 'bqty', 'layn', '5fcd2d7942c5480f3580925696e57da42e415eff', '', 'alafootom@msn.com', 0, 0, 5489),
(0, 'alain', 'bqty', 'layn', '5fcd2d7942c5480f3580925696e57da42e415eff', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 5489),
(0, 'alain', 'bqty', 'layn', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 5489),
(0, 'alain', 'bqty', 'layn', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', 'alafootom@msn.com', 0, 0, 5489),
(0, 'alain', 'bqty', 'layn', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'monkeyd', 'alafootom@msn.com', 665265987, 89, 54894),
(0, 'jjj', 'jjj', 'jjj', 'c84c766f873ecedf75aa6cf35f1e305e095fec83', '', 'jjj@mm.com', 0, 0, 656565);

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

CREATE TABLE `professeur` (
  `ID` int(11) NOT NULL,
  `username` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Prenom` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `Nom` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `adresse` varchar(255) COLLATE latin1_general_cs NOT NULL,
  `telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `eleves`
--
ALTER TABLE `eleves`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `eleves`
--
ALTER TABLE `eleves`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
